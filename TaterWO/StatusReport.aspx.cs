﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TaterWO
{
    public partial class StatusReport : System.Web.UI.Page
    {
        #region Private Variable
        private String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        #region Bind Grid       

        private void GridOrderStatus(int StatusTypeID)
        {
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetStatusReport";
            cmd.Parameters.Add("@StatusTypeID", SqlDbType.Int).Value = StatusTypeID;
            cmd.Connection = con;
            try
            {
                con.Open();
                gdOrder.EmptyDataText = "No Records Found";
                gdOrder.DataSource = cmd.ExecuteReader();
                gdOrder.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion

        protected void ddlOrderStatus_SelectedIndexChanged(object sender, EventArgs e)
        {           
            string OrderStatus_Id = ddlOrderStatus.SelectedValue;
            int OrderStatusId = Convert.ToInt32(OrderStatus_Id);
            if (OrderStatusId != 0)
            {
                GridOrderStatus(OrderStatusId);
            }            
        }
    }
}