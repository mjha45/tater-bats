﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Log.Utils.Logging;
using Log.Utils.Email;
using Log.Config;
using Log.Utils.DataAccess;

namespace TaterWO.Config
{
    class AppSettings : Log.Config.Settings
    {
        #region Static
        #region Private
        private static AppSettings _ref;
        #endregion
        #region Public Properties
        public static AppSettings Ref
        {
            get
            {
                if (_ref == null)
                {
                    try
                    {
                        _ref = new AppSettings();
                    }
                    catch (Exception ex)
                    {
                        System.Diagnostics.Debug.WriteLine(ex.Message);
                    }
                }
                return _ref;
            }
        }
        #endregion
        #endregion

        #region Events
        #endregion

        #region Private Variables
        private Database _database;
        private bool _runMinimized;
        private bool _runUnattended = false;
        private int _defaultMinutesBeforeNotification;
        private string _notifyEmailAddress;
        private List<string> _switchCommands;
        private int _NumberVoidsToRun;
        #endregion

        #region Constructor
        private AppSettings()
            : base(false)
        {
            //Make this a private constructor, so that this is a singleton object.
            _switchCommands = new List<string>();
            _runMinimized = false;
            _runUnattended = false;
        }
        #endregion

        #region Public Properties
        public static string[] GetArguments()
        {
            return Environment.GetCommandLineArgs();
        }
        public Database Database
        {
            get { return _database; }
            set { _database = value; }
        }
        public bool RunMinimized
        {
            get { return _runMinimized; }
            set { _runMinimized = value; }
        }
        public bool RunUnattended
        {
            get { return _runUnattended; }
            set { _runUnattended = value; }
        }
        public int DefaultMinutesBeforeNotification
        {
            get { return _defaultMinutesBeforeNotification; }
            set { _defaultMinutesBeforeNotification = value; }
        }
        public string NotifyEmailAddress
        {
            get { return _notifyEmailAddress; }
            set { _notifyEmailAddress = value; }
        }
        public List<string> SwitchCommands
        {
            get { return _switchCommands; }
            set { _switchCommands = value; }
        }
        public int NumberVoidsToRun
        {
            get { return _NumberVoidsToRun; }
            set { _NumberVoidsToRun = value; }
        }
        #endregion

        #region Private Methods
        #endregion

        #region Public Methods
        #region Override Methods
        public override bool ReadProgramSettings()
        {
            try
            {
                //Get the Log Controller, and associated logs
                LogControler.Ref = (LogControler)System.Configuration.ConfigurationManager.GetSection("LogController");

                ////Get the email object
                Emailer.Ref = (Emailer)System.Configuration.ConfigurationManager.GetSection("Emailer");

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
            }
        }
        public override void ReadCommandArguments(string[] args)
        {
            //CommandLineReader reader = new CommandLineReader(args);
            foreach (string cmdValue in args)
            {
                //Check if we are running the application minimized
                if (string.Compare(cmdValue.Replace("-", string.Empty).ToUpper(), "MIN", true) == 0)
                    _runMinimized = true;
                //Check if we are running Unattended
                else if (string.Compare(cmdValue.Replace("-", string.Empty).ToUpper(), "U", true) == 0)
                    _runUnattended = true;
                else
                {
                    _switchCommands.Add(cmdValue.Replace("-", string.Empty).ToUpper());
                }
            }
        }
        public override void SpitOutCommandLineHelp()
        {
            string version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            string name = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            string dispText = name;
            dispText += " (Version:" + version.ToString() + ")" + Environment.NewLine + Environment.NewLine;
            dispText += "Arguments:" + Environment.NewLine;
            dispText += "-MIN -->Run Minimized" + Environment.NewLine;
            dispText += "-U   -->Run Unattended" + Environment.NewLine;
            dispText += "-C   -->Run Clean Process" + Environment.NewLine;
            dispText += "-V   -->Run Void Process" + Environment.NewLine;
            dispText += "-R   -->Run Reset Process" + Environment.NewLine;
            dispText += "-AS  -->Run Account Setup Process" + Environment.NewLine;
            dispText += "-AA  -->Run Keep Accounts Active Process" + Environment.NewLine;
            MessageBox.Show(dispText, "Command Line Parameters", MessageBoxButtons.OK, MessageBoxIcon.Question);
        }
        public override bool LogMessage(string message)
        {
            try
            {
                return base.LogMessage(message);
            }
            catch { }
            return false;
        }
        public new bool LogError(Exception exception, ErrorLevel errorLevel)
        {
            return base.LogError(exception, errorLevel);
        }
        #endregion
        #endregion
    }
}
