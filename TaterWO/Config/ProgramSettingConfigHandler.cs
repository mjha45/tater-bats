﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;

namespace TaterWO.Config
{
    class ProgramSettingConfigHandler : IConfigurationSectionHandler
    {
        #region Private Variables
        #endregion

        #region Constructor
        public ProgramSettingConfigHandler()
        {
        }
        public ProgramSettingConfigHandler(XmlNode parent)
        {
            foreach (XmlNode node in parent.ChildNodes)
            {
                if (string.Compare("NumberVoidsToRun", node.Name, true) == 0)
                {
                    int value=1000;
                    int.TryParse(node.InnerText, out value);
                    Config.AppSettings.Ref.NumberVoidsToRun = value;
                }
            }
        }
        #endregion

        #region Public Properties
        #endregion

        #region IConfigurationSectionHandler Members
        /// <summary>
        /// Create method is used by IConfigurationSectionHandler to build an object from XML in config section
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, XmlNode section)
        {
            ProgramSettingConfigHandler settings = new ProgramSettingConfigHandler(section);
            return settings;
        }
        #endregion

    }
}
