﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;
using TaterWO.Model;
namespace TaterWO
{
    public partial class OrderStatusReport : System.Web.UI.Page
    {
        #region Private Variable
        private OrderInventoryEntities db = new OrderInventoryEntities();
        private String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        int FilledId = 0;
        int OrderStatusId = 0;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                BindStatus();
            }
        }

        private void BindStatus()
        {
            var status = (from s in db.Status select s).ToList();
            ddlStatus.DataValueField = "StatusID";
            ddlStatus.DataTextField = "Status1";
            ddlStatus.DataSource = status;
            DataBind();
        }

        private void BindFilled()
        {
            string query = "select StatusTypeID, StatusType from StatusType";
            BindDropDownList(ddlFilled, query, "StatusType", "StatusTypeID", "--Select Filled Type--");
            //ddlFilled.Items.Insert(0, new ListItem("Select Filled Type", "0"));
            //var statusType = (from s in db.StatusTypes select s).ToList();
            //ddlFilled.DataValueField = "StatusTypeID";
            //ddlFilled.DataTextField = "StatusType1";
            //ddlFilled.DataSource = statusType;
            //DataBind();
        }

        private void BindDropDownList(DropDownList ddl, string query, string text, string value, string defaultText)
        {
            SqlCommand cmd = new SqlCommand(query);
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    con.Open();
                    ddl.DataSource = cmd.ExecuteReader();
                    ddl.DataTextField = text;
                    ddl.DataValueField = value;
                    ddl.DataBind();
                    con.Close();
                }
            }
            ddl.Items.Insert(0, new ListItem(defaultText, "0"));
        }

        #region Bind Grid       

        private void GridOrderStatus(int StatusID, int StatusTypeID)
        {
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetStatusWiseReport";
            cmd.Parameters.Add("@StatusID", SqlDbType.Int).Value = StatusID;
            cmd.Parameters.Add("@StatusTypeID", SqlDbType.Int).Value = StatusTypeID;
            cmd.Connection = con;
            try
            {
                con.Open();
                gdOrder.EmptyDataText = "No Records Found";
                gdOrder.DataSource = cmd.ExecuteReader();
                gdOrder.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }
        #endregion      

        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            string OrderStatus_Id = ddlStatus.SelectedValue;
            OrderStatusId = Convert.ToInt32(OrderStatus_Id);
            Session["OrderStatusId"] = OrderStatusId;
            if (OrderStatusId > 1)
            {
                BindFilled();
                GridOrderStatus(0, 0);
            }
        }

        protected void ddlFilled_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Order_Status_Id = 0;
            if (Session["OrderStatusId"] != null)
            {
                Order_Status_Id = Convert.ToInt32(Session["OrderStatusId"]);
                string Filled_Id = ddlFilled.SelectedValue;
                FilledId = Convert.ToInt32(Filled_Id);
                if (FilledId > 0)
                {
                    GridOrderStatus(Order_Status_Id, FilledId);
                }
            }            
        }
    }
}