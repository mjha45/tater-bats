﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StatusReport.aspx.cs" Inherits="TaterWO.StatusReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <tr>
        <td colspan="3" style="text-align: left;">
            Status Type: 
            <asp:DropDownList ID="ddlOrderStatus" AutoPostBack="True" AppendDataBoundItems="true" Width="250px" runat="server" OnSelectedIndexChanged="ddlOrderStatus_SelectedIndexChanged">
                <asp:ListItem Text="--Select--" Value="0" />
                <asp:ListItem Text="Unfulfilled" Value="1" />
                <asp:ListItem Text="Fulfilled" Value="2" />
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
            <br />
            <div class="section_box">
                <div id="section_title">&nbsp;&nbsp;&nbsp;Filled Wise Report&nbsp;&nbsp;&nbsp;</div>
                <div id="section_content">
                    <asp:GridView ID="gdOrder" runat="server" AutoGenerateColumns="False" CssClass="grid" Width="100%" CellPadding="2" CellSpacing="2">
                        <Columns>
                            <asp:BoundField DataField="PurchaseOrderID" HeaderText="Order ID" SortExpression="PurchaseOrderID" Visible="false" />
                            <asp:BoundField DataField="OrderNum" HeaderText="Order#" SortExpression="OrderNum" />
                            <asp:BoundField DataField="OrderType" HeaderText="Type" SortExpression="OrderType" />
                            <asp:BoundField DataField="CustomerName" HeaderText="Customer" SortExpression="CustomerName" />
                            <asp:BoundField DataField="BatModel" HeaderText="Model" SortExpression="BatModel" />
                            <asp:BoundField DataField="BatWood" HeaderText="Wood" SortExpression="BatWood" />
                            <asp:BoundField DataField="BarrelColor" HeaderText="Barrel" SortExpression="BarrelColor" />
                            <asp:BoundField DataField="HandleColor" HeaderText="Handle" SortExpression="HandleColor" />
                            <asp:BoundField DataField="ProductSize" HeaderText="Size" SortExpression="ProductSize" />
                            <asp:BoundField DataField="BatWeight" HeaderText="Weight" SortExpression="BatWeight" />
                            <asp:BoundField DataField="Cupping" HeaderText="Cupping" SortExpression="Cupping" />
                            <asp:BoundField DataField="OrderQty" HeaderText="Order Qty" SortExpression="OrderQty" />
                            <asp:BoundField DataField="ReleaseQty" HeaderText="Delivered" SortExpression="ReleaseQty" />
                            <asp:BoundField DataField="RemeaningQty" HeaderText="UnDelivered" SortExpression="RemeaningQty" />
                            <asp:BoundField DataField="UnitPrice" HeaderText="Price" SortExpression="UnitPrice" />
                            <asp:BoundField DataField="TotalPrice" HeaderText="Total Price" SortExpression="TotalPrice" />
                            <asp:BoundField DataField="StatusType" HeaderText="Status" SortExpression="StatusType" />
                        </Columns>
                    </asp:GridView>
                </div>
                <br />
            </div>
        </td>
    </tr>
</asp:Content>
