﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OrderDetails.aspx.cs" Inherits="TaterWO.OrderDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <tr>
        <td colspan="3" style="text-align: center">
            <h3>Update Order</h3>
        </td>
    </tr>

    <tr>
        <td colspan="3">
            <br />
            <br />
            <div class="section_box">
                <div id="section_title">&nbsp;&nbsp;&nbsp;Order Details for &nbsp;:&nbsp;&nbsp;<asp:Label ID="lblCustomer_Name" runat="server" Text=""></asp:Label></div>
                <div id="section_content">
                    <table cellpadding="6" style="width: 100%;">
                        <tr>
                            <td style="text-align: right; width: 15%;">Model: </td>
                            <td style="text-align: left; width: 33%;">
                                <asp:DropDownList ID="ddlModelType" Width="200px" runat="server" AutoPostBack="True" DataTextField="BatModel" DataValueField="BatModelID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlModelType" InitialValue="1" ErrorMessage="Select Model"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>
                            </td>
                            <td style="width: 4%;">&nbsp;</td>
                            <td style="text-align: right; width: 15%;">Wood: </td>
                            <td style="text-align: left; width: 33%;">
                                <asp:DropDownList ID="ddlWood" Width="200px" runat="server" AutoPostBack="True" DataTextField="BatWood" DataValueField="BatWoodID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlWood" InitialValue="1" ErrorMessage="Select Wood"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Barrel Color: </td>
                            <td style="text-align: left;">
                                <asp:DropDownList ID="ddlBarrelColor" Width="200px" runat="server" AutoPostBack="True" DataTextField="ColorName" DataValueField="ColorID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlBarrelColor" InitialValue="1" ErrorMessage="Select Color"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Handle Color: </td>
                            <td>
                                <asp:DropDownList ID="ddlHandleColor" Width="200px" runat="server" AutoPostBack="True" DataTextField="ColorName" DataValueField="ColorID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlHandleColor" InitialValue="1" ErrorMessage="Select Color"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Cupping: </td>
                            <td style="text-align: left;">
                                <asp:DropDownList ID="ddlCupping" AutoPostBack="True" Width="200px" runat="server">
                                    <asp:ListItem Text="--Select--" Value="0" />
                                    <asp:ListItem Text="Cupped" Value="Cupped" />
                                    <asp:ListItem Text="Uncupped" Value="Uncupped" />
                                </asp:DropDownList>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlCupping" InitialValue="0" ErrorMessage="Select Cupping"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Length: </td>
                            <td>
                                <asp:DropDownList ID="ddlSize" Width="200px" runat="server" AutoPostBack="True" DataTextField="ProductSize" DataValueField="ProductSizeID"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlSize" InitialValue="1" ErrorMessage="Select Length"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Qty: </td>
                            <td style="text-align: left;">                                
                                <asp:Label ID="txtQty" runat="server" Text=""></asp:Label>
                                <%--<asp:RequiredFieldValidator id="RequiredFieldValidator8" runat="server" ControlToValidate="txtQty" ErrorMessage="Enter Qty" ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Bat Weight: </td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtBatWeight" Width="200px" runat="server"></asp:TextBox>
                                 <asp:RequiredFieldValidator id="RequiredFieldValidator9" runat="server" ControlToValidate="txtBatWeight" ErrorMessage="Enter Weight" ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Price: </td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtUnitPrice" Width="200px" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator id="RequiredFieldValidator10" runat="server" ControlToValidate="txtUnitPrice" ErrorMessage="Enter Price" ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Billet Weight : </td>
                            <td>
                                <asp:TextBox ID="txtBilletWeight" Width="200px" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Option Notes: </td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtOption" Width="200px" Height="30px" TextMode="MultiLine" runat="server"></asp:TextBox></td>
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Engraving: </td>
                            <td style="text-align: left;">
                                <asp:RadioButtonList ID="rdEngraving" runat="server">
                                    <asp:ListItem Text="Yes" Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Text="No" Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Bat Notes: </td>
                            <td>
                                <asp:TextBox ID="txtBatNotes" Width="200px" Height="30px" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Engraving Desc: </td>
                            <td>
                                <asp:TextBox ID="txtEngravingDesc" Width="200px" Height="30px" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Deliver Qty: </td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtDeliverQty" Width="200px" runat="server"></asp:TextBox>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Order Status: </td>
                            <td style="text-align: left;">
                                 <asp:DropDownList ID="ddlOrderStatus" AutoPostBack="True" Width="200px" runat="server">
                                    <asp:ListItem Text="--Select--" Value="0" />
                                    <asp:ListItem Text="Unfulfilled" Value="1" />
                                    <asp:ListItem Text="Fulfilled" Value="2" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" style="text-align: center;">
                                <asp:Button ID="btnUpdateOrder" Width="100px" runat="server" Text="Update Order" OnClick="btnUpdateOrder_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>

    
</asp:Content>
