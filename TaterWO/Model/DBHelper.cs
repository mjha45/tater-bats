﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TaterWO.Model
{
    public class DBHelper
    {
        //Validate the user from DB
        public static bool CheckUser(string username, string password)
        {
            DataTable result = null;
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "select Password from Employees where username = @username";
                        cmd.Parameters.Add(new SqlParameter("@username", username));

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            result = new DataTable();
                            da.Fill(result);
                        }

                        if (password.Trim() == result.Rows[0]["Password"].ToString().Trim())
                        {
                            //user id found and password is matched too so lets do soemthing now
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Pokemon exception handling
            }

            //user id not found, lets treat him as a guest        
            return false;
        }

        //Get the Roles for this particular user
        public static string GetUserRoles(string username)
        {
            DataTable result = null;
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "select RoleID from Employees where username = @username";
                        cmd.Parameters.Add(new SqlParameter("@username", username));

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            result = new DataTable();
                            da.Fill(result);
                        }

                        if (result.Rows.Count == 1)
                        {
                            return result.Rows[0]["RoleID"].ToString().Trim();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Pokemon exception handling
            }

            //user id not found, lets treat him as a guest        
            return "guest";
        }
    }
}