﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using TaterWO.Model;
namespace TaterWO
{
    public partial class OrderDetails : System.Web.UI.Page
    {
        #region Private Variable
        public int Order_Id = 0;

        private OrderInventoryEntities db = new OrderInventoryEntities();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Response.Redirect("http://www.example.com/rendernews.php?id=" + ID);
                if (!IsPostBack)
                {
                    string OrderId = Request.QueryString["id"].ToString();

                    Session["OrderID"] = OrderId;

                    Order_Id = Convert.ToInt32(OrderId);
                    BindAllDropDownList();

                    var Order = (from o in db.PurchaseOrders where o.PurchaseOrderID == Order_Id select o).FirstOrDefault();
                    txtQty.Text =Convert.ToString( Order.OrderQty);
                    //BindModel();
                    string ModelId = Convert.ToString(Order.BatModelID);
                    ddlModelType.SelectedIndex = ddlModelType.Items.IndexOf(ddlModelType.Items.FindByValue(ModelId));

                    //BindWood();
                    string WoodId = Convert.ToString(Order.BatWoodID);
                    ddlWood.SelectedIndex = ddlWood.Items.IndexOf(ddlWood.Items.FindByValue(WoodId));

                    //BindLength();
                    string SizeId = Convert.ToString(Order.ProductSizeID);
                    ddlSize.SelectedIndex = ddlSize.Items.IndexOf(ddlSize.Items.FindByValue(SizeId));

                    //BindBarrelColor();
                    string BarrelColorId = Convert.ToString(Order.BarrelColor);
                    ddlBarrelColor.SelectedIndex = ddlBarrelColor.Items.IndexOf(ddlBarrelColor.Items.FindByValue(BarrelColorId));

                    //BindHandleColor();
                    string HandleColorId = Convert.ToString(Order.ColorID);
                    ddlHandleColor.SelectedIndex = ddlHandleColor.Items.IndexOf(ddlHandleColor.Items.FindByValue(HandleColorId));

                    string cupping = Convert.ToString(Order.Cupping);
                    ddlCupping.SelectedIndex = ddlCupping.Items.IndexOf(ddlCupping.Items.FindByValue(cupping));

                    txtQty.Text = Convert.ToString(Order.OrderQty);
                    txtBatWeight.Text = Order.BatWeight;
                    txtUnitPrice.Text = Convert.ToString(Order.UnitPrice);
                    txtBilletWeight.Text = Order.BilletWeight;
                    txtOption.Text = Order.OptionName;
                    txtEngravingDesc.Text = Order.EngravingDesc;
                    var customername = (from c in db.Customers where c.CustomerID == Order.CustomerID select c).FirstOrDefault();
                    lblCustomer_Name.Text = customername.CustomerName;
                    if (Order.Engraving == true)
                    {
                        rdEngraving.Items[0].Selected = true;
                        //rdEngraving.SelectedIndex = 1;
                    }
                    else
                    {
                        rdEngraving.Items[0].Selected = false;
                        //rdEngraving.SelectedIndex = 0;
                    }
                    int OrderStatusId = (int)Order.StatusTypeID;
                    string OrderStatus_Id = Convert.ToString(OrderStatusId);
                    if (OrderStatusId != 0)
                    {
                        ddlOrderStatus.SelectedIndex = ddlOrderStatus.Items.IndexOf(ddlOrderStatus.Items.FindByValue(OrderStatus_Id));
                    }
                }
            }
            catch(Exception ex)
            {
                Config.AppSettings.Ref.LogMessage(string.Concat("Error Page Load: ", ex));
            }
        }

        protected void btnUpdateOrder_Click(object sender, EventArgs e)
        {
            Config.AppSettings.Ref.ReadProgramSettings();
            try
            {
                #region Local Variables
                int Size_Id = 0;
                int Barrel_Color_Id = 0;
                int Handle_Color_Id = 0;
                int ReleaseQty = 0;
                int OrderQty = 0;
                int UnitPrice = 0;
                int Wood_Id = 0;
                int ModelType_Id = 0;
                int OrderStatus_Id = 0;
                string Bat_Weight = "";
                string Cupping = "";
                #endregion
                Order_Id = Convert.ToInt32(Session["OrderID"]);
                //using (OrderInventoryEntities context = new OrderInventoryEntities(DBConnection().getConnectionString()))
                //{
                //    myRecord con = context.PurchaseOrders.First(item => item.PurchaseOrderID == Order_Id); //Gets the record succesfully, PK field in tact
                //    con.ApprovalStatus = approvalCode;

                //    context.SubmitChanges();
                //}

                // PurchaseOrder order = db.PurchaseOrders.Single(u => u.PurchaseOrderID == Order_Id);
                PurchaseOrder order = db.PurchaseOrders.Find(Order_Id);


                //var order = (from po in db.PurchaseOrders where po.PurchaseOrderID == Order_Id select po).FirstOrDefault();
                if (ddlSize.SelectedValue != null)
                {
                    string SizeID = ddlSize.SelectedValue;
                    Size_Id = Convert.ToInt32(SizeID);
                    order.ProductSizeID = Size_Id;  //Get Size ID
                }
                else
                {
                    order.ProductSizeID = Size_Id;
                }

                if (ddlBarrelColor.SelectedValue != null)
                {
                    string ColorID = ddlBarrelColor.SelectedValue;
                    Barrel_Color_Id = Convert.ToInt32(ColorID);
                    order.BarrelColor = Barrel_Color_Id;  //Get Color ID
                }
                else
                {
                    order.BarrelColor = Barrel_Color_Id;
                }

                if (ddlHandleColor.SelectedValue != null)
                {
                    string ColorID = ddlHandleColor.SelectedValue;
                    Handle_Color_Id = Convert.ToInt32(ColorID);
                    order.ColorID = Handle_Color_Id;  //Get Color ID
                }
                else
                {
                    order.ColorID = Handle_Color_Id;
                }

                if (ddlModelType.SelectedValue != null)
                {
                    string ModelTypeID = ddlModelType.SelectedValue;
                    ModelType_Id = Convert.ToInt32(ModelTypeID);
                    order.BatModelID = ModelType_Id; //Get Model
                }
                else
                {
                    order.BatModelID = ModelType_Id; //Get Model
                }

                if (ddlWood.SelectedValue != null)
                {
                    string WoodID = ddlWood.SelectedValue;
                    Wood_Id = Convert.ToInt32(WoodID);
                    order.BatWoodID = Wood_Id; //Get Wood
                }
                else
                {
                    order.BatWoodID = Wood_Id; //Get Wood
                }

                Bat_Weight = txtBatWeight.Text;
                order.BatWeight = Bat_Weight;

                string Order_Qty = txtQty.Text;
                if (Order_Qty != "" || Order_Qty != "0")
                {
                    OrderQty = Convert.ToInt32(Order_Qty);
                }

               // order.OrderQty = OrderQty;

                string Release_Qty = txtDeliverQty.Text;
                if (Release_Qty != "" || Release_Qty != "0")
                {
                    ReleaseQty = Convert.ToInt32(Release_Qty);
                }

                order.ReleaseQty = ReleaseQty;
                //order.ReleaseQty = txtDeliverQty.Text;
                order.RemeaningQty = OrderQty - ReleaseQty;

                //string Unit_Price = txtUnitPrice.Text;
                //if (Unit_Price != "" || Unit_Price != "0")
                //{
                //    UnitPrice = Convert.ToInt32(Unit_Price);
                //}

                //order.UnitPrice = UnitPrice;
                //if (OrderQty > 0 && UnitPrice > 0)
                //{
                //    order.TotalPrice = OrderQty * UnitPrice;
                //}
                //else
                //{
                //    order.TotalPrice = 0;
                //}
                //if (txtOnlineOrderNumber.Text != "" || txtOnlineOrderNumber.Text != null)
                //{
                //    order.OnlineOrder = txtOnlineOrderNumber.Text;
                //}
                //else
                //{
                //    order.OnlineOrder = "0";
                //}
                if (ddlCupping.SelectedValue != "0")
                {
                    Cupping = ddlCupping.SelectedValue;
                }
                order.Cupping = Cupping;
                order.BilletWeight = txtBilletWeight.Text;
                bool Engraving;
                if (rdEngraving.SelectedValue == "Yes")
                {
                    Engraving = true;
                }
                else
                {
                    Engraving = false;
                }

                if (ddlOrderStatus.SelectedValue != null)
                {
                    string OrderStatusId = ddlOrderStatus.SelectedValue;
                    OrderStatus_Id = Convert.ToInt32(OrderStatusId);
                    order.StatusTypeID = OrderStatus_Id; 
                }
                else
                {
                    order.StatusTypeID = OrderStatus_Id; 
                }

                order.Engraving = Engraving;
                order.EngravingDesc = txtEngravingDesc.Text;

                db.SaveChanges();

       

            }
            catch (Exception ex)
            {
                Config.AppSettings.Ref.LogMessage(string.Concat("Error Update Order: ", ex));
            }
        }

        #region DropDown
        private void BindAllDropDownList()
        {
            BindModel();
            BindWood();
            BindBarrelColor();
            BindHandleColor();
            BindLength();
        }

        private void BindModel()
        {
            var Model = (from o in db.BatModels select o).ToList();
            ddlModelType.DataValueField = "BatModelID";
            ddlModelType.DataTextField = "BatModel1";
            ddlModelType.DataSource = Model;
            DataBind();
            //ddlModelType.SelectedIndex = ddlModelType.Items.IndexOf(ddlModelType.Items.FindByValue(ModelId));
        }

        private void BindWood()
        {
            var Wood = (from o in db.BatWoods select o).ToList();
            ddlWood.DataValueField = "BatWoodID";
            ddlWood.DataTextField = "BatWood1";
            ddlWood.DataSource = Wood;
            DataBind();
        }

        private void BindLength()
        {
            var Size = (from o in db.ProductSizes select o).ToList();
            ddlSize.DataValueField = "ProductSizeID";
            ddlSize.DataTextField = "ProductSize1";
            ddlSize.DataSource = Size;
            DataBind();
        }

        private void BindBarrelColor()
        {
            var Color = (from o in db.Colors where o.ColorName != "" select o).ToList();
            ddlBarrelColor.DataValueField = "ColorID";
            ddlBarrelColor.DataTextField = "ColorName";
            ddlBarrelColor.DataSource = Color;
            DataBind();
        }

        private void BindHandleColor()
        {
            var Color = (from o in db.Colors where o.ColorName != "" select o).ToList();
            ddlHandleColor.DataValueField = "ColorID";
            ddlHandleColor.DataTextField = "ColorName";
            ddlHandleColor.DataSource = Color;
            DataBind();
        }
        #endregion
    }
}