﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OrderStatus.aspx.cs" Inherits="TaterWO.OrderStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <tr>
        <td colspan="3" style="text-align: center">
            <h3>Update Order Status</h3>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
            <br />
            <div class="section_box">
                <div id="section_title">
                    &nbsp;&nbsp;&nbsp;Order Details for &nbsp;:&nbsp;&nbsp;
                    <asp:Label ID="lblCustomer_Name" runat="server" Text=""></asp:Label>
                </div>
                <div id="section_content">
                    <table cellpadding="6" style="width: 100%;">
                        <tr>
                            <td style="text-align: right; width: 10%;">Order Dated: </td>
                            <td style="text-align: left; width: 25%; margin-left: 120px;">
                                <asp:Label ID="lblOrder_Date" runat="server" Text=""></asp:Label>
                            </td>
                            <td style="text-align: right; width: 25%;">Order Quantitiy: </td>
                            <td style="text-align: left; width: 10%;">
                                <asp:Label ID="lblOrder_Qty" runat="server" Text=""></asp:Label>
                            </td>
                            <td style="width: 1%;">&nbsp;</td>
                            <td style="text-align: right; width: 10%;">Order Status: </td>
                            <td style="text-align: left; width: 20%;">
                                <asp:Label ID="lblOrder_Status" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="7">
                                <asp:GridView ID="GridView1" Width="100%" runat="server" AutoGenerateColumns="false" DataKeyNames="StatusOrderID" OnRowCancelingEdit="GridView1_RowCancelingEdit"
                                    OnRowEditing="GridView1_RowEditing" OnRowDataBound="GridView1_RowDataBound" OnRowUpdating="GridView1_RowUpdating">
                                    <Columns>
                                        <asp:TemplateField HeaderText="StatusOrderID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatusOrderID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StatusOrderID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Label ID="lblEditStatusOrderID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "StatusOrderID") %>'></asp:Label>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:TextBox ID="txtAddStatusOrderID" runat="server"></asp:TextBox>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Serial#">
                                            <ItemTemplate>
                                                <%# Eval("ItemID") %>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Label ID="lblItemID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ItemID") %>'></asp:Label>
                                            </EditItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Order Number">
                                            <ItemTemplate>
                                                <%# Eval("OrderNum") %>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Label ID="lblOrderNum" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "OrderNum") %>'></asp:Label>
                                            </EditItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <%# Eval("Status") %>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="Status" DataTextField="Status" DataValueField="StatusID" runat="server"></asp:DropDownList>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnedit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" Text="Delete" Visible="false"></asp:LinkButton>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:LinkButton ID="btnUpdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                                <asp:LinkButton ID="btncancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</asp:Content>
