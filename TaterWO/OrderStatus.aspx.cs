﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using TaterWO.Model;

namespace TaterWO
{
    public partial class OrderStatus : System.Web.UI.Page
    {
        #region Private Variable        
        string conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        private OrderInventoryEntities db = new OrderInventoryEntities();
        public int Order_Id = 0;
        string Customer_Name = "";
        string Order_Qty = "";
        string Order_Date = "";
        string Order_Status = "";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            string OrderId = Request.QueryString["id"].ToString();
            
            if (OrderId != null || OrderId != "")
            {
                Order_Id = Convert.ToInt32(OrderId);
                GetOrderDetails();

                if (!IsPostBack)
                {
                    BindData();
                }
            }
        }

        private void GetOrderDetails()
        {
            string sqlStr = "select c.CustomerName, p.OrderQty, p.CreatedOn, p.StatusTypeID from PurchaseOrder p, Customer c where p.CustomerID = c.CustomerID and p.PurchaseOrderID =" + Order_Id;
            SqlConnection con = new SqlConnection(conStr);
            SqlCommand cmd = new SqlCommand(sqlStr, con);
            SqlDataReader reader;
            try
            {
                con.Open();
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Customer_Name = Convert.ToString(reader["CustomerName"]);
                    lblCustomer_Name.Text = Customer_Name;
                    Order_Qty = Convert.ToString(reader["OrderQty"]);
                    lblOrder_Qty.Text = Order_Qty;
                    DateTime OrdersDate = Convert.ToDateTime(reader["CreatedOn"]);
                    Order_Date = OrdersDate.ToString("d");
                    lblOrder_Date.Text = Order_Date;
                    Order_Status = Convert.ToString(reader["StatusTypeID"]);
                    if (Order_Status == "1")
                    {
                        lblOrder_Status.Text = "Unfulfilled";
                    }
                    else
                    {
                        lblOrder_Status.Text = "Fulfilled";
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        #region GridView
        private void BindData()
        {
            GridView1.DataSource = DataBindByDataSet();
            GridView1.DataBind();
        }


        private DataSet DataBindByDataSet()
        {
            string sqlStr = "select StatusOrderID, OrderID,ItemID, CAST(OrderNumber AS VARCHAR(10)) + '-' + OrderRevision as OrderNum,Status from StatusOrderEntry so, Status s where so.StatusID = s.StatusID and OrderID =" + Order_Id;
            DataSet ds = new DataSet();
            SqlConnection con = new SqlConnection(conStr);

            con.Open();
            SqlCommand cmd = new SqlCommand(sqlStr, con);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(ds);

            con.Close();
            return ds;
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            BindData();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            SqlConnection conn = new SqlConnection(conStr);
            Label lblEditStatusOrderID = (Label)GridView1.Rows[e.RowIndex].FindControl("lblEditStatusOrderID");
            DropDownList ddlStatusID = (DropDownList)GridView1.Rows[e.RowIndex].FindControl("Status") as DropDownList;
            string status_ID = ddlStatusID.SelectedValue;
            int statusID = Convert.ToInt32(status_ID);
            DateTime ItemStartDate = DateTime.Now;

            conn.Open();
            string cmdstr = "update StatusOrderEntry set StatusID=@StatusId, ItemStartDate ='" + ItemStartDate + "' where StatusOrderID=@StatusOrderID";
            SqlCommand cmd = new SqlCommand(cmdstr, conn);
            cmd.Parameters.AddWithValue("@StatusOrderID", lblEditStatusOrderID.Text);
            cmd.Parameters.AddWithValue("@StatusId", statusID);
            cmd.ExecuteNonQuery();

            conn.Close();
            GridView1.EditIndex = -1;
            BindData();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            BindData();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if ((e.Row.RowState & DataControlRowState.Edit) > 0)
                {
                    DropDownList StatusDesc = (DropDownList)e.Row.FindControl("Status");

                    string sqlStr = "SELECT StatusID, Status FROM Status";
                    DataSet ds = new DataSet();
                    SqlConnection con = new SqlConnection(conStr);

                    con.Open();
                    SqlCommand cmd = new SqlCommand(sqlStr, con);
                    SqlDataAdapter ad = new SqlDataAdapter(cmd);
                    ad.Fill(ds);

                    con.Close();
                    StatusDesc.DataSource = ds;
                    StatusDesc.DataBind();

                    StatusDesc.DataTextField = "Status";
                    StatusDesc.DataValueField = "StatusID";

                    DataRowView dr = e.Row.DataItem as DataRowView;
                    StatusDesc.SelectedValue = dr["Status"].ToString();
                }
            }
        }
        #endregion
    }
}