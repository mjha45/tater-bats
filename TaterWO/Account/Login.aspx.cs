﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using TaterWO.Model;

namespace TaterWO.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //RegisterHyperLink.NavigateUrl = "Register";
            //OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];

            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                //RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
        }

        protected void Login_Click(object sender, EventArgs e)
        {
            //http://www.codeproject.com/Articles/408306/Understanding-and-Implementing-ASP-NET-Custom-Form
            string roles;
            TextBox userName = (TextBox)LoginView1.FindControl("UserName");
            TextBox Password = (TextBox)LoginView1.FindControl("Password");
            if (userName != null)
            {
                string uname = userName.Text;
                string password = Password.Text;

                //string username = UserName;
                if (DBHelper.CheckUser(uname, password) == true)
                {
                    //These session values are just for demo purpose to show the user details on master page
                    Session["User"] = uname;
                    roles = DBHelper.GetUserRoles(uname);
                    Session["Roles"] = roles;

                    //Let us now set the authentication cookie so that we can use that later.
                    FormsAuthentication.SetAuthCookie(uname, false);

                    //Login successful lets put him to requested page
                    string returnUrl = Request.QueryString["ReturnUrl"] as string;

                    if (returnUrl != null)
                    {
                        Response.Redirect(returnUrl);
                    }
                    else
                    {
                        //no return URL specified so lets kick him to home page
                        Response.Redirect("~/Default.aspx");
                    }
                }
                else
                {
                    //Label1.Text = "Your login attempt was not successful. Please try again.";
                }
            }
        }
    }
}