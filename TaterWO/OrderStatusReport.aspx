﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OrderStatusReport.aspx.cs" Inherits="TaterWO.OrderStatusReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <tr>
        <td style="text-align: left; width: 50%;">&nbsp;&nbsp;&nbsp;Status Type&nbsp;:&nbsp;&nbsp;
            <asp:DropDownList ID="ddlStatus" Width="250px" runat="server" AutoPostBack="True" DataTextField="Status1" DataValueField="StatusID" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"></asp:DropDownList>
        </td>
        <td></td>
        <td style="text-align: left; width: 50%;">Filled Type&nbsp;:&nbsp;&nbsp;
            <asp:DropDownList ID="ddlFilled" Width="250px" runat="server" AutoPostBack ="true" OnSelectedIndexChanged ="ddlFilled_SelectedIndexChanged"></asp:DropDownList>
            <%--<asp:DropDownList ID="ddlFilled" Width="250px" runat="server" AutoPostBack="True" DataTextField="StatusType1" DataValueField="StatusTypeID" OnSelectedIndexChanged="ddlFilled_SelectedIndexChanged"></asp:DropDownList>          
            <asp:DropDownList ID="ddlFilled" AutoPostBack="True" Width="250px" runat="server" OnSelectedIndexChanged="ddlFilled_SelectedIndexChanged">                
                <asp:ListItem Text="Unfulfilled" Value="1" />
                <asp:ListItem Text="Fulfilled" Value="2" />
            </asp:DropDownList>--%>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
            <br />
            <div class="section_box">
                <div id="section_title">&nbsp;&nbsp;&nbsp;Status Wise Report&nbsp;&nbsp;&nbsp;</div>
                <div id="section_content">
                    <asp:GridView ID="gdOrder" runat="server" AutoGenerateColumns="False" CssClass="grid" Width="100%" CellPadding="2" CellSpacing="2">
                        <Columns>
                            <asp:BoundField DataField="PurchaseOrderID" HeaderText="Order ID" SortExpression="PurchaseOrderID" Visible="false" />
                            <asp:BoundField DataField="OrderNum" HeaderText="Order#" SortExpression="OrderNum" />
                            <asp:BoundField DataField="ItemID" HeaderText="Item#" SortExpression="ItemID" />
                            <asp:BoundField DataField="OrderType" HeaderText="Type" SortExpression="OrderType" />
                            <asp:BoundField DataField="CustomerName" HeaderText="Customer" SortExpression="CustomerName" />
                            <asp:BoundField DataField="BatModel" HeaderText="Model" SortExpression="BatModel" />
                            <asp:BoundField DataField="BatWood" HeaderText="Wood" SortExpression="BatWood" />
                            <asp:BoundField DataField="BarrelColor" HeaderText="Barrel" SortExpression="BarrelColor" />
                            <asp:BoundField DataField="HandleColor" HeaderText="Handle" SortExpression="HandleColor" />
                            <asp:BoundField DataField="ProductSize" HeaderText="Size" SortExpression="ProductSize" />
                            <asp:BoundField DataField="BatWeight" HeaderText="Weight" SortExpression="BatWeight" />
                            <asp:BoundField DataField="Cupping" HeaderText="Cupping" SortExpression="Cupping" />
                            <asp:BoundField DataField="OrderQty" HeaderText="Order Qty" SortExpression="OrderQty" Visible="false"/>
                            <asp:BoundField DataField="ReleaseQty" HeaderText="Delivered" SortExpression="ReleaseQty" Visible="false"/>
                            <asp:BoundField DataField="RemeaningQty" HeaderText="UnDelivered" SortExpression="RemeaningQty" Visible="false"/>
                            <asp:BoundField DataField="StatusType" HeaderText="Status" SortExpression="StatusType" />
                        </Columns>
                    </asp:GridView>
                </div>
                <br />
            </div>
        </td>
    </tr>
</asp:Content>
