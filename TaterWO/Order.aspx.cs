﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;
using TaterWO.Model;

namespace TaterWork
{
    public partial class Order : System.Web.UI.Page
    {
        #region Private Variable
        int Customre_Id = 0;
        private OrderInventoryEntities db = new OrderInventoryEntities();
        private String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        #endregion

        #region Main Method
        protected void Page_Load(object sender, EventArgs e)
        {
            // Config.AppSeting.Ref.ReadProgramSettings();
            try
            {
                if (!IsPostBack)
                {
                    BindAllDropDownList();
                    string CustomerID = ddlCustomer.SelectedValue;
                    Customre_Id = Convert.ToInt32(CustomerID);
                    GridOrderByCustomer(Customre_Id);
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

        #region Dropdown List Bind

        private void BindOrderType()
        {
            var Order_Type = (from o in db.OrderTypes select o).ToList();

            //var Order_Type = from c in db.OrderTypes  select new { c.OrderTypeID, c.OrderType1 };
            ddlOrderType.DataValueField = "OrderTypeID";
            ddlOrderType.DataTextField = "OrderType1";
            ddlOrderType.DataSource = Order_Type;
            // ddlOrderType.Items.Insert(0, "--Select--");
            DataBind();

            //ListItem listItem = new ListItem("-Please Select-", "Item Value");

            //ddlOrderType.Items.Insert(0, listItem);
            //ddlOrderType.SelectedIndex = 0;
            //ListItem myDefaultItem = new ListItem("(select)", string.Empty);
            //myDefaultItem.Selected = true;
            //ddlOrderType.Items.Insert(0, myDefaultItem);

            //ddlOrderType.Items.Insert(0, new ListItem("-- Select --", String.Empty));
        }

        private void BindCustomer()
        {
            var Customer = (from c in db.Customers select c).ToList();
            ddlCustomer.DataValueField = "CustomerID";
            ddlCustomer.DataTextField = "CustomerName";
            ddlCustomer.DataSource = Customer;
            DataBind();
        }

        private void BindModel()
        {
            var Model = (from o in db.BatModels select o).ToList();
            ddlModelType.DataValueField = "BatModelID";
            ddlModelType.DataTextField = "BatModel1";
            ddlModelType.DataSource = Model;
            DataBind();
        }

        private void BindWood()
        {
            var Wood = (from o in db.BatWoods select o).ToList();
            ddlWood.DataValueField = "BatWoodID";
            ddlWood.DataTextField = "BatWood1";
            ddlWood.DataSource = Wood;
            DataBind();
        }

        private void BindLength()
        {
            var Size = (from o in db.ProductSizes select o).ToList();
            ddlSize.DataValueField = "ProductSizeID";
            ddlSize.DataTextField = "ProductSize1";
            ddlSize.DataSource = Size;
            DataBind();
        }

        private void BindBarrelColor()
        {
            var Color = (from o in db.Colors where o.ColorName != "" select o).ToList();
            ddlBarrelColor.DataValueField = "ColorID";
            ddlBarrelColor.DataTextField = "ColorName";
            ddlBarrelColor.DataSource = Color;
            DataBind();
        }

        private void BindHandleColor()
        {
            var Color = (from o in db.Colors where o.ColorName != "" select o).ToList();
            ddlHandleColor.DataValueField = "ColorID";
            ddlHandleColor.DataTextField = "ColorName";
            ddlHandleColor.DataSource = Color;
            DataBind();
        }

        private void BindAllDropDownList()
        {
            BindOrderType();
            BindCustomer();
            BindModel();
            BindWood();
            BindLength();
            BindBarrelColor();
            BindHandleColor();
        }
        #endregion

        #region Button Click Event
        protected void btnOrder_Click(object sender, EventArgs e)
        {
            try
            {
                #region Save Customre
                //Customer customer = new Customer();
                //customer.CustomerName = txtCustomerName.Text;
                //customer.Email = txtEmail.Text;
                //customer.CustomerPhone = txtPhone.Text;
                //customer.CustomerMobile = "";
                //customer.CustomerFax = "";
                //customer.CustomerAddress = txtAddress.Text;
                //customer.CustomerState = "";
                //customer.CustomerZip = "";
                //customer.CustomerCountry = "";
                //db.Customers.Add(customer);
                //db.SaveChanges();
                #endregion

                if (ddlCustomer.SelectedValue != "1")
                {
                    #region Local Variables
                    int Size_Id = 0;
                    int Barrel_Color_Id = 0;
                    int Handle_Color_Id = 0;
                    Int32 lOrderNum = 0;
                    string lOrderRevision = "";
                    int OrderQty = 0;
                    int Customer_ID = 0;
                    int UnitPrice = 0;
                    int Wood_Id = 0;
                    int ModelType_Id = 0;
                    string Bat_Weight = "";
                    string Cupping = "";
                    #endregion

                    #region Order
                    PurchaseOrder order = new PurchaseOrder();

                    #region OrderNumber
                    if (ddlCustomer.SelectedIndex != -1)
                    {
                        string CustomerID = ddlCustomer.SelectedValue;
                        Customer_ID = Convert.ToInt32(CustomerID);
                    }
                    string lastOrderRevision = "";
                    var Order = (from o in db.PurchaseOrders where o.CustomerID == Customer_ID select o).ToList();
                    int ordercount = Order.Count();
                    if (ordercount > 0)
                    {
                        foreach (PurchaseOrder pOrder in Order)
                        {
                            lOrderNum = pOrder.OrderNumber;
                            lastOrderRevision = pOrder.OrderRevision;
                        }
                        lOrderRevision = IncrementRevision(lastOrderRevision);
                    }
                    else
                    {
                        var MaxOrderNum = db.PurchaseOrders.OrderByDescending(u => u.OrderNumber).FirstOrDefault();
                        if (MaxOrderNum == null)
                        {
                            lOrderNum = 1;
                            lOrderRevision = "a";
                        }
                        else
                        {
                            lOrderNum = MaxOrderNum.OrderNumber;
                            lOrderNum++;
                            lOrderRevision = "a";
                        }
                    }
                    #endregion
                    order.OrderNumber = lOrderNum;
                    order.OrderRevision = lOrderRevision;

                    DateTime UTCDateTime = DateTime.UtcNow;
                    DateTime CreateOn = UTCDateTime.ToLocalTime();

                    order.CustomerID = Customer_ID; // customer.CustomerID;

                    if (ddlSize.SelectedValue != null)
                    {
                        string SizeID = ddlSize.SelectedValue;
                        Size_Id = Convert.ToInt32(SizeID);
                        order.ProductSizeID = Size_Id;  //Get Size ID
                    }
                    else
                    {
                        order.ProductSizeID = Size_Id;
                    }

                    if (ddlBarrelColor.SelectedValue != null)
                    {
                        string ColorID = ddlBarrelColor.SelectedValue;
                        Barrel_Color_Id = Convert.ToInt32(ColorID);
                        order.BarrelColor = Barrel_Color_Id;  //Get Color ID
                    }
                    else
                    {
                        order.BarrelColor = Barrel_Color_Id;
                    }

                    if (ddlHandleColor.SelectedValue != null)
                    {
                        string ColorID = ddlHandleColor.SelectedValue;
                        Handle_Color_Id = Convert.ToInt32(ColorID);
                        order.ColorID = Handle_Color_Id;  //Get Color ID
                    }
                    else
                    {
                        order.ColorID = Handle_Color_Id;
                    }

                    int OrderType_Id = 0;
                    if (ddlOrderType.SelectedValue != null)
                    {
                        string OrderTypeID = ddlOrderType.SelectedValue;
                        OrderType_Id = Convert.ToInt32(OrderTypeID);
                        order.OrderTypeID = OrderType_Id; //Get OrderType
                    }
                    else
                    {
                        order.OrderTypeID = OrderType_Id; //Get OrderType
                    }

                    if (ddlModelType.SelectedValue != null)
                    {
                        string ModelTypeID = ddlModelType.SelectedValue;
                        ModelType_Id = Convert.ToInt32(ModelTypeID);
                        order.BatModelID = ModelType_Id; //Get Model
                    }
                    else
                    {
                        order.BatModelID = ModelType_Id; //Get Model
                    }

                    if (ddlWood.SelectedValue != null)
                    {
                        string WoodID = ddlWood.SelectedValue;
                        Wood_Id = Convert.ToInt32(WoodID);
                        order.BatWoodID = Wood_Id; //Get Wood
                    }
                    else
                    {
                        order.BatWoodID = Wood_Id; //Get Wood
                    }

                    Bat_Weight = txtBatWeight.Text;
                    order.BatWeight = Bat_Weight;

                    string Order_Qty = txtQty.Text;
                    if (Order_Qty != "" || Order_Qty != "0")
                    {
                        OrderQty = Convert.ToInt32(Order_Qty);
                    }

                    order.OrderQty = OrderQty;
                    order.ReleaseQty = 0;
                    order.RemeaningQty = OrderQty;
                    order.CreatedOn = CreateOn;
                    int employeeID = 1;// Convert.ToInt32(Session["EmployeeID"]);
                    order.CreatedBy = employeeID;

                    string Unit_Price = txtUnitPrice.Text;
                    if (Unit_Price != "" || Unit_Price != "0")
                    {
                        UnitPrice = Convert.ToInt32(Unit_Price);
                    }

                    order.UnitPrice = UnitPrice;
                    if (OrderQty > 0 && UnitPrice > 0)
                    {
                        order.TotalPrice = OrderQty * UnitPrice;
                    }
                    else
                    {
                        order.TotalPrice = 0;
                    }
                    if (txtOnlineOrderNumber.Text != "" || txtOnlineOrderNumber.Text != null)
                    {
                        order.OnlineOrder = txtOnlineOrderNumber.Text;
                    }
                    else
                    {
                        order.OnlineOrder = "0";
                    }
                    if (ddlCupping.SelectedValue != "0")
                    {
                        Cupping = ddlCupping.SelectedValue;
                    }
                    order.Cupping = Cupping;
                    order.BilletWeight = txtBilletWeight.Text;
                    bool Engraving;
                    if (rdEngraving.SelectedValue == "Yes")
                    {
                        Engraving = true;
                    }
                    else
                    {
                        Engraving = false;
                    }
                    order.Engraving = Engraving;
                    order.EngravingDesc = txtEngravingDesc.Text;
                    order.OptionName = txtOption.Text;
                    order.Description = txtBatNotes.Text;
                    order.StatusTypeID = 1;
                    db.PurchaseOrders.Add(order);
                    db.SaveChanges();
                    #endregion

                    #region Item
                    //Item objItem = new Item();
                    //objItem.ItemName = "";
                    //objItem.SizeID = Size_Id;
                    //objItem.ColorID = Handle_Color_Id;
                    //objItem.OrderID = order.PurchaseOrderID;
                    //objItem.OrderNumber = Convert.ToString(lOrderNum);
                    //objItem.OrderRevision = order.OrderRevision;
                    //objItem.Qty = OrderQty;
                    //objItem.CreatedOn = CreateOn;
                    //objItem.CreatedBy = employeeID;
                    //objItem.Description = txtBatNotes.Text;
                    //objItem.OptionName = txtOption.Text;
                    //objItem.CustomerID = Customer_ID;
                    //objItem.UnitPrice = UnitPrice;
                    //objItem.BatModelID = ModelType_Id;
                    //objItem.BatWoodID = Wood_Id;
                    //objItem.BatWeight = Bat_Weight;
                    //objItem.BarrelColor = Barrel_Color_Id;
                    //objItem.Cupping = Cupping;
                    //db.Items.Add(objItem);
                    //db.SaveChanges();
                    #endregion

                    #region Inventory
                    //Inventory objInventory = new Inventory();
                    //objInventory.OrderID = order.PurchaseOrderID;
                    //objInventory.OrderNumber = Convert.ToString(lOrderNum);
                    //objInventory.OrderRevision = order.OrderRevision;

                    ////Opening Inventory Quantity
                    //objInventory.Qty = 0;

                    ////Order Quantity
                    //objInventory.InvoiceQty = OrderQty;

                    ////Reamening Inventory Quantity
                    //objInventory.RemainingQty = 0;
                    //objInventory.CreatedOn = CreateOn;
                    //objInventory.CreatedBy = employeeID;
                    #endregion

                    #region StatusUpdate

                    for (int i = 1; i < OrderQty + 1; i++)
                    {
                        StatusOrderEntry status = new StatusOrderEntry();
                        status.StatusID = 2;
                        status.OrderID = order.PurchaseOrderID;
                        status.ItemID = i;
                        status.ItemStartDate = CreateOn;
                        status.ItemCompletionDate = CreateOn;
                        status.StatusTypeID = 1;
                        status.OrderNumber = lOrderNum;
                        status.OrderRevision = lOrderRevision;
                        db.StatusOrderEntries.Add(status);
                        db.SaveChanges();
                    }
                    #endregion

                    #region Bind Grid
                    BindAllDropDownList();
                    ClearText();
                    GridOrderByCustomer(Customer_ID);
                    #endregion
                }
                else
                {
                    // Display the error message

                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnNewCustomer_Click(object sender, EventArgs e)
        {
            try
            {
                #region Save Customre
                Customer customer = new Customer();
                customer.CustomerName = txtCustomerName.Text;
                customer.Email = txtEmail.Text;
                customer.CustomerPhone = txtPhone.Text;
                customer.CustomerMobile = "";
                customer.CustomerFax = "";
                customer.CustomerAddress = txtAddress.Text;
                customer.CustomerState = "";
                customer.CustomerZip = "";
                customer.CustomerCountry = "";
                db.Customers.Add(customer);
                db.SaveChanges();

                BindCustomer();
                ddlCustomer.SelectedIndex = ddlCustomer.Items.IndexOf(ddlCustomer.Items.FindByText(txtCustomerName.Text));
                ClearText();
                #endregion
            }
            catch (Exception ex) { }
        }

        protected void btnBarrel_Click(object sender, EventArgs e)
        {
            Color color = new Color();
            if (txtBarrel.Text != "" || txtBarrel.Text != null)
            {
                color.ColorName = txtBarrel.Text;
                db.Colors.Add(color);
                db.SaveChanges();
                BindBarrelColor();

                ddlBarrelColor.SelectedIndex = ddlBarrelColor.Items.IndexOf(ddlBarrelColor.Items.FindByText(txtBarrel.Text));
                txtBarrel.Text = "";
            }
            else
            {
                //display error message
            }
        }

        protected void btnHandle_Click(object sender, EventArgs e)
        {
            Color color = new Color();
            if (txtHandle.Text != "" || txtHandle.Text != null)
            {
                color.ColorName = txtHandle.Text;
                db.Colors.Add(color);
                db.SaveChanges();
                BindHandleColor();

                ddlHandleColor.SelectedIndex = ddlHandleColor.Items.IndexOf(ddlHandleColor.Items.FindByText(txtHandle.Text));
                txtHandle.Text = "";
            }
            else
            {
                //display error message
            }
        }

        protected void btnLength_Click(object sender, EventArgs e)
        {
            ProductSize size = new ProductSize();
            if (txtLength.Text != "" || txtLength.Text != null)
            {
                size.ProductSize1 = txtLength.Text;
                db.ProductSizes.Add(size);
                db.SaveChanges();
                BindLength();

                ddlSize.SelectedIndex = ddlSize.Items.IndexOf(ddlSize.Items.FindByText(txtLength.Text));
                txtLength.Text = "";
            }
            else
            {
                //display error message
            }
        }

        protected void btnAddModel_Click(object sender, EventArgs e)
        {
            BatModel batModel = new BatModel();
            if (txtModel.Text != "" || txtModel.Text != null)
            {
                batModel.BatModel1 = txtModel.Text;
                db.BatModels.Add(batModel);
                db.SaveChanges();
                BindModel();

                ddlModelType.SelectedIndex = ddlModelType.Items.IndexOf(ddlModelType.Items.FindByText(txtModel.Text));
                txtModel.Text = "";
            }
            else
            {
                //display error message
            }
        }
        #endregion

        #region Common Method
        private void ClearText()
        {
            txtCustomerName.Text = "";
            txtPhone.Text = "";
            txtEmail.Text = "";
            txtAddress.Text = "";
            txtQty.Text = "";
            txtUnitPrice.Text = "";
            txtBatWeight.Text = "";
            txtBilletWeight.Text = "";
            txtOption.Text = "";
            txtBatNotes.Text = "";
            txtEngravingDesc.Text = "";
        }

        static string IncrementRevision(string value)
        {
            if (value.All(ch => ch == 'z'))
            {
                return new string('a', value.Length + 1);
            }
            var res = value.ToCharArray();
            var position = res.Length - 1;
            do
            {
                if (res[position] != 'z')
                {
                    res[position]++;
                    break;
                }
                res[position--] = 'a';
            } while (true);
            return new string(res);
        }
        #endregion

        #region Bind Grid       

        private void GridOrderByCustomer(int Customre_Id)
        {
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "GetOrderByCustomer";
            cmd.Parameters.Add("@CustomerID", SqlDbType.Int).Value = Customre_Id;// txtID.Text.Trim();
            cmd.Connection = con;
            try
            {
                con.Open();
                gdOrder.EmptyDataText = "No Records Found";
                gdOrder.DataSource = cmd.ExecuteReader();
                gdOrder.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        private void GridOrder()
        {
            var Order = (from o in db.PurchaseOrders select o).ToList();
            gdOrder.DataSource = Order;
            gdOrder.DataBind();
        }
        #endregion

        #region DropDown Selected Index Changed
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            string CustomreId = ddlCustomer.SelectedValue;
            Customre_Id = Convert.ToInt32(CustomreId);
            GridOrderByCustomer(Customre_Id);

            //BindPurchaseOrder(Customre_Id);
        }

        //protected void ddlOrderType_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    var cityQuery = from c in we.city
        //                    where c.CountryCode == ddlOrderType.SelectedValue
        //                    orderby c.Name
        //                    select new { c.Name, c.Population, c.CountryCode };
        //    GridView1.DataSource = cityQuery;
        //    DataBind();
        //}
        #endregion
    }
}