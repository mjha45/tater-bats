﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Order.aspx.cs" Inherits="TaterWork.Order" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script lang="javascript">
        function ShowHideDivBarrel() {
            $('#divBarrel').toggle("slow");
            return false;
        }

        function ShowHideDivHandle() {
            $('#divHandle').toggle("slow");
            return false;
        }

        function ShowHideDivModel() {
            $('#divModel').toggle("slow");
            return false;
        }

        function ShowHideDivCustomer() {
            $('#divCustomer').toggle("slow");
            return false;
        }
        function ShowHideDivLength() {
            $('#divLength').toggle("slow");
            return false;
        }

    </script>
    <tr>
        <td colspan="3" style="text-align: center">
            <h3>Purchase Order</h3><br />
        </td>        
    </tr>
    <tr>
        <td colspan="3">
            <div class="section_box">
                <div id="section_title">&nbsp;&nbsp;&nbsp;Customer&nbsp;&nbsp;&nbsp;</div>
                <div id="section_content">
                    <table cellpadding="6" style="width: 100%;">
                        <tr>
                            <td style="text-align: right; width: 10%;">Order Type: </td>
                            <td style="text-align: left; width: 12%; margin-left: 120px;">
                                <asp:DropDownList ID="ddlOrderType" Width="150px" runat="server" AutoPostBack="True" DataTextField="OrderType" DataValueField="OrderTypeID"></asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlOrderType" InitialValue="1" ErrorMessage="Select Order Type"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td style="text-align: right; width: 10%;">Online Order#: </td>
                            <td style="text-align: left; width: 13%;">
                                <asp:TextBox ID="txtOnlineOrderNumber" Width="150px" runat="server" TabIndex="1"></asp:TextBox>
                            </td>
                            <td style="width: 1%;">&nbsp;</td>
                            <td style="text-align: right; width: 10%;">Customer Name: </td>
                            <td style="text-align: left; width: 22%;">
                                <asp:DropDownList ID="ddlCustomer" Width="150px" runat="server" AutoPostBack="True" DataTextField="CustomerName" DataValueField="CustomerID" OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged" TabIndex="2">
                                </asp:DropDownList>
                                <asp:Button ID="btndivCustomer" runat="server" Text="+" ToolTip="Add Customer" OnClientClick="javascript:return ShowHideDivCustomer();" />
                            </td>
                        </tr>
                      </table>
                    <div id="divCustomer" style="display: none;">
                        <table cellpadding="6" style="width: 100%; border: 1px dotted #808080;">
                            <tr>
                                <td style="text-align: right; width: 15%;">Customer Name: </td>
                                <td style="text-align: left; width: 33%;">
                                    <asp:TextBox ID="txtCustomerName" Width="250px" runat="server"></asp:TextBox></td>
                                <td style="width: 4%;">&nbsp;</td>
                                <td style="text-align: right; width: 15%;">Email: </td>
                                <td style="text-align: left; width: 33%;">
                                    <asp:TextBox ID="txtEmail" Width="250px" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td style="text-align: right;">Phone: </td>
                                <td>
                                    <asp:TextBox ID="txtPhone" Width="250px" runat="server"></asp:TextBox></td>
                                <td>&nbsp;</td>
                                <td style="text-align: right;">Address: </td>
                                <td>
                                    <asp:TextBox ID="txtAddress" Width="250px" runat="server"></asp:TextBox>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                                <td style="text-align: left;">
                                    <asp:Button ID="btnNewCustomer" runat="server" Text="Add New Customer" OnClick="btnNewCustomer_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </td>
    </tr>

    <tr>
        <td colspan="3">
            <br />
            <br />
            <div class="section_box">
                <div id="section_title">&nbsp;&nbsp;&nbsp;Purchase Order&nbsp;&nbsp;&nbsp;</div>
                <div id="section_content">
                    <table cellpadding="6" style="width: 100%;">
                        <tr>
                            <td style="text-align: right; width: 15%;">Model: </td>
                            <td style="text-align: left; width: 33%;">
                                <asp:DropDownList ID="ddlModelType" Width="220px" runat="server" AutoPostBack="True" DataTextField="BatModel" DataValueField="BatModelID"></asp:DropDownList>
                                <asp:Button ID="btnDivModel" runat="server" Text="+" ToolTip="Add New Model" OnClientClick="javascript:return ShowHideDivModel();" />
                                <div id="divModel" style="display: none;">
                                    <asp:TextBox ID="txtModel" Width="150px" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnAddModel" runat="server" Text="Add Model" OnClick="btnAddModel_Click" />
                                </div>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlModelType" InitialValue="1" ErrorMessage="Select Model"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td style="width: 4%;">&nbsp;</td>
                            <td style="text-align: right; width: 15%;">Wood: </td>
                            <td style="text-align: left; width: 33%;">
                                <asp:DropDownList ID="ddlWood" Width="250px" runat="server" AutoPostBack="True" DataTextField="BatWood" DataValueField="BatWoodID"></asp:DropDownList>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlWood" InitialValue="1" ErrorMessage="Select Wood"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Barrel Color: </td>
                            <td style="text-align: left;">
                                <asp:DropDownList ID="ddlBarrelColor" Width="220px" runat="server" AutoPostBack="True" DataTextField="ColorName" DataValueField="ColorID"></asp:DropDownList>
                                <asp:Button ID="btnDivBarrel" runat="server" Text="+" ToolTip="Add New Barrel" OnClientClick="javascript:return ShowHideDivBarrel();" />
                                <div id="divBarrel" style="display: none;">
                                    <asp:TextBox ID="txtBarrel" Width="150px" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnBarrel" runat="server" Text="Add Barrel" OnClick="btnBarrel_Click" />
                                </div>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlBarrelColor" InitialValue="1" ErrorMessage="Select Color"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Handle Color: </td>
                            <td>
                                <asp:DropDownList ID="ddlHandleColor" Width="220px" runat="server" AutoPostBack="True" DataTextField="ColorName" DataValueField="ColorID"></asp:DropDownList>
                                <asp:Button ID="btnDivHandle" runat="server" Text="+" ToolTip="Add New Handle" OnClientClick="javascript:return ShowHideDivHandle();" />
                                <div id="divHandle" style="display: none;">
                                    <asp:TextBox ID="txtHandle" Width="150px" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnHandle" runat="server" Text="Add Handle" OnClick="btnHandle_Click" />
                                </div>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlHandleColor" InitialValue="1" ErrorMessage="Select Color"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Cupping: </td>
                            <td style="text-align: left;">
                                <asp:DropDownList ID="ddlCupping" AutoPostBack="True" AppendDataBoundItems="true" Width="250px" runat="server">
                                    <asp:ListItem Text="--Select--" Value="0" />
                                    <asp:ListItem Text="Cupped" Value="Cupped" />
                                    <asp:ListItem Text="Uncupped" Value="Uncupped" />
                                </asp:DropDownList>
                                 <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlCupping" InitialValue="0" ErrorMessage="Select Cupping"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Length: </td>
                            <td>
                                <asp:DropDownList ID="ddlSize" Width="220px" runat="server" AutoPostBack="True" DataTextField="ProductSize" DataValueField="ProductSizeID"></asp:DropDownList>
                                <asp:Button ID="btnDivSize" runat="server" Text="+" ToolTip="Add New Length" OnClientClick="javascript:return ShowHideDivLength();" />
                                <div id="divLength" style="display: none;">
                                    <asp:TextBox ID="txtLength" Width="150px" runat="server"></asp:TextBox>
                                    <asp:Button ID="btnLength" runat="server" Text="Add Length" OnClick="btnLength_Click" />
                                </div>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlSize" InitialValue="1" ErrorMessage="Select Length"
             ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>--%>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Qty: </td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtQty" Width="250px" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator id="RequiredFieldValidator8" runat="server" ControlToValidate="txtQty" ErrorMessage="Enter Qty" ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Bat Weight: </td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtBatWeight" Width="250px" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator id="RequiredFieldValidator9" runat="server" ControlToValidate="txtBatWeight" ErrorMessage="Enter Weight" ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>--%>
                            </td>                              
                        </tr>                        
                        <tr>
                            <td style="text-align: right;">Price: </td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtUnitPrice" Width="250px" runat="server"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator id="RequiredFieldValidator10" runat="server" ControlToValidate="txtUnitPrice" ErrorMessage="Enter Price" ForeColor="Red" Font-Names="Impact"></asp:RequiredFieldValidator>--%>
                            </td>                            
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Billet Weight : </td>
                            <td>
                                <asp:TextBox ID="txtBilletWeight" Width="250px" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Option Notes: </td>
                            <td style="text-align: left;">
                                <asp:TextBox ID="txtOption" Width="250px" Height="30px" TextMode="MultiLine" runat="server"></asp:TextBox></td>
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Engraving: </td>
                            <td style="text-align: left;">
                                <asp:RadioButtonList ID="rdEngraving" runat="server">
                                    <asp:ListItem Text="Yes" Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Text="No" Value="No">No</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">Bat Notes: </td>
                            <td>
                                <asp:TextBox ID="txtBatNotes" Width="250px" Height="30px" TextMode="MultiLine" runat="server"></asp:TextBox>
                            </td>                            
                            <td>&nbsp;</td>
                            <td style="text-align: right;">Engraving Desc: </td>
                            <td>
                                <asp:TextBox ID="txtEngravingDesc" Width="250px" Height="30px" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </tr>
                        
                        <tr>
                            <td colspan="5" style="text-align: center;">
                                <asp:Button ID="btnOrder" Width="100px" runat="server" Text="Add Order" OnClick="btnOrder_Click" />
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>

    <tr>
        <td colspan="3">
            <br />
            <br />
            <div class="section_box">
                <div id="section_title">&nbsp;&nbsp;&nbsp;Order Details&nbsp;&nbsp;&nbsp;</div>
                <div id="section_content">
                    <asp:GridView ID="gdOrder" runat="server" AutoGenerateColumns="False" CssClass="grid" Width="100%" CellPadding="2" CellSpacing="2">
                        <Columns>
                            <asp:BoundField DataField="PurchaseOrderID" HeaderText="Order ID" SortExpression="PurchaseOrderID" Visible="false" />
                            <asp:hyperlinkfield datatextfield="OrderNum" datanavigateurlfields="PurchaseOrderID" datanavigateurlformatstring="~\OrderDetails.aspx?Id={0}"  />
                            <%--<asp:BoundField DataField="OrderNum" HeaderText="Order" SortExpression="OrderNum" />--%>
                            <asp:BoundField DataField="OrderType" HeaderText="Type" SortExpression="OrderType" />
                            <asp:BoundField DataField="CustomerName" HeaderText="Customer" SortExpression="CustomerName" />
                            <asp:BoundField DataField="BatModel" HeaderText="Model" SortExpression="BatModel" />
                            <asp:BoundField DataField="BatWood" HeaderText="Wood" SortExpression="BatWood" />
                            <asp:BoundField DataField="BarrelColor" HeaderText="Barrel" SortExpression="BarrelColor" />
                            <asp:BoundField DataField="HandleColor" HeaderText="Handle" SortExpression="HandleColor" />
                            <asp:BoundField DataField="ProductSize" HeaderText="Size" SortExpression="ProductSize" />
                            <asp:BoundField DataField="BatWeight" HeaderText="Weight" SortExpression="BatWeight" />
                            <asp:BoundField DataField="Cupping" HeaderText="Cupping" SortExpression="Cupping" />
                            <asp:BoundField DataField="OrderQty" HeaderText="Order Qty" SortExpression="OrderQty" />
                            <asp:BoundField DataField="ReleaseQty" HeaderText="Delivered" SortExpression="ReleaseQty" />
                            <asp:BoundField DataField="RemeaningQty" HeaderText="UnDelivered" SortExpression="RemeaningQty" />
                            <asp:BoundField DataField="UnitPrice" HeaderText="Price" SortExpression="UnitPrice" />
                            <asp:BoundField DataField="TotalPrice" HeaderText="Total Price" SortExpression="TotalPrice" />
                            <asp:hyperlinkfield Text="Status" datanavigateurlfields="PurchaseOrderID" datanavigateurlformatstring="~\OrderStatus.aspx?Id={0}"  />
                        </Columns>
                    </asp:GridView>
                    <%--<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OrderInventoryConnectionString %>" SelectCommand="SELECT [OrderNumber], [OrderRevision], [ProductHandleID], [ProductTitleID], [ProductSizeID], [ColorID], [OrderQty], [BatWoodID], [BatModelID], [CustomerID], [UnitPrice], [TotalPrice] FROM [PurchaseOrder]"></asp:SqlDataSource>--%>
                </div>
                <br />
            </div>
        </td>
    </tr>
</asp:Content>
